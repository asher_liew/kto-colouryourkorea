<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="description" content="Show us your creativity by participating in the #colouryourkorea Colouring Contest.">
        <title>KTO - #colouryourkorea</title>

        <link rel="icon" type="image/png" href="{{asset('assets/images/website%image.png')}}"/>
        <!-- Fonts -->
        <link href="{{asset('css/fonts.css')}}" rel="stylesheet">
        
        <!-- Styles -->        
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <!-- <link rel="stylesheet" href="{{asset('css/all.min.css')}}"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="{{asset('css/desktop.css')}}" rel="stylesheet">
        <link href="{{asset('css/mobile.css')}}" rel="stylesheet">
        <link href="{{asset('css/dropzone.min.css')}}" rel="stylesheet">
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */
            html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}

            .dropzone{
                background: none;
                border: none;
                padding: 0;
            }

        </style>
    </head>
    <body id="bg" class="antialiased">
        <div id="bg-texture"></div>


         
        
        

        <!-- Home -->
        <div class="position-relative mx-auto w-100" id="home">  
          <!-- Navigation -->
            <nav>
                <div class="text-center">      
                    <img id="logo" src="{{asset('assets/images/KTO-logo-header.png')}}" alt="colouryourkorea"/>
                </div>
                    <div id="navbar" class="text-center mx-auto">
                        <div class="row justify-content-center align-items-center">
                            <div class="col"><a href="#home"><img class="button-img" src="{{asset('assets/images/Home-button.png')}}"/></a></div>
                            <div class="col"><a href="#prizes"><img class="button-img" src="{{asset('assets/images/Prizes-button.png')}}"/></a></div>
                            <div class="col"><a href="#how-to-win"><img class="button-img" src="{{asset('assets/images/How-to-win-button.png')}}"/></a></div>
                            <div class="col"><a href="#gallery"><img class="button-img" src="{{asset('assets/images/Gallery.png')}}"/></a></div>
                        </div>
                    </div>
            </nav> 
            <!-- End Navigation -->
            
            <img id="hero-image" src="{{asset('assets/images/KTO_OG-Image-2.png')}}" alt="KTO">  
            <div class="container" id="home-content">  
            <div class="w-100 text-right"><img id="tag-image" src="{{asset('assets/images/Flag-Desktop.png')}}" alt="KTO"></div>

                <div class="mx-auto pb-8 text-center section mb-md-5" id="home-text">
                    <p class="my-0">Unleash your Korea-tivity and bring your idea of Korea to life!</p>
                    <p class="my-0">Join the<span id="tag">#colouryourkorea</span> contest and show us your creativity by submitting your artwork. Get creative and happy colouring!</p>
                    <p  class="d-none d-md-block text-bold mt-5 mb-0 mt-md-4">Contest Period: 2 November 2020 to 30 November 2020</p>
                    <p class="d-block d-md-none text-bold mt-5 mb-0">Contest Period: 
                    <span class="text-bold d-none d-md-block">2 November 2020 to 30 November 2020</span>
                    <p class="text-bold my-0 d-block d-md-none">2 November 2020 to 30 November 2020</p>
                    </p>
                    <div class="mx-auto mt-5 pt-5 mt-md-2 pt-md-2">
                        <a href="#join-now" id="join-now-button">
                            <img class="button-img desktop-display" src="{{asset('assets/images/mobile/Join-now-button.png')}}" alt="KTO join now"/>
                            <img class="button-img mobile-display" src="{{asset('assets/images/mobile/Join-now-button.png')}}" alt="KTO join now"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Home -->

         <!-- Mobile Only Grass & Stone  -->
         <div class="my- mobile-stone-grass text-right"><img id="mobile-stone-grass-1" src="{{asset('assets/images/mobile/Stone-element-1.png')}}"/></div>
        <!-- End Mobile Only Grass & Stone  -->

       
        <div id="prizes" class="mx-auto section position-relative">
            <img id="watercolour-1" class="position-absolute watercolor" src="{{asset('assets/images/Watercolour-1.png')}}" alt="Watercolour 1">
            <div class="content-box">
                <h1 class="blue-title text-center py-0 mb-2">Prizes</h1>
                <p class="text-center">Join now and stand a chance to win these attractive prizes!</p>
                <div class="my-4">
                    <div class="text-md-center display-flex">
                        <div class="prizes-sample">
                            <div class="text-center prizes-flex-left px-0"><img id="prize-image-1" src="{{asset('assets/images/Grand-prize.png')}}"></div>
                            <div class="prizes-flex-right">
                                <h2 class="subtitle">Grand Prize</h2>
                                <p class="prizes-text">Samsung Galaxy <br class="d-block d-md-none">Tab S7</p>
                                <p class="prizes-text">(Worth RM 3,299)</p>
                            </div>
                        </div>
                        <div class="prizes-sample">
                            <div class="text-center prizes-flex-left"><img src="{{asset('assets/images/Consolation-prize.png')}}"></div>
                            <div class="prizes-flex-right">
                                <h2 class="subtitle">Consolation Prizes</h2>
                                <p class="prizes-text">LANEIGE Essential Care Trial set X 10</p>
                                <p class="prizes-text">(Worth RM 1600)</p>
                            </div>
                        </div>
                        <div class="prizes-sample">
                            <div class="text-center prizes-flex-left px-0"><img id="prize-image-3" src="{{asset('assets/images/Paticipating-prize.png')}}"></div>
                            <div class="prizes-flex-right">
                                <h2 class="subtitle">Participating Prizes</h2>
                                <p class="prizes-text">Care Package with Innisfree Hand Cream, Mask Case, and Covid Care Kit</p>
                                <p class="prizes-text">(Worth RM 60)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img id="watercolour-2" class="position-absolute watercolor" src="{{asset('assets/images/Watercolour-2.png')}}" alt="Watercolour 1">

        </div>
        <!-- Mobile Only Grass & Stone  -->
        <div class="mobile-stone-grass mt-2"><img id="mobile-stone-grass-2" class="pl-4" src="{{asset('assets/images/mobile/Stone-element-2.png')}}" /></div>
        <!-- End Mobile Only Grass & Stone  -->   

        <br/> 
        <div id="how-to-win" class="mx-auto section position-relative">
            <div class="mx-auto content-box">
                <h1 class="blue-title text-center mb-5">How to Win</h1>
                    <div class="row mx-auto no-gutters">
                        <!-- download postcard -->
                        <div class="col-12 col-md mb-5">
                            <div class="row col-sm-12 col-md justify-content-center align-items-center mx-auto px-0">
                                <h1 class="col-12 col-md-5 subtitle d-none d-md-block text-right pr-0">Step 1:</h1>
                                <div class="col-12 col-md-7 text-center text-md-left px-0"><img class="how-to-win-image pl-md-2" src="{{asset('assets/images/Step-1-icon.png')}}"></div>
                            </div>
                            <div class="d-flex col-sm-12 col-md justify-content-center align-items-center text-center mt-2 mx-auto step-description">
                                    <h1 class="col-2 subtitle d-block d-md-none">Step 1:</h1>
                                    <p class="col-10 col-md-12 how-to-win-instruction"><a class="btn how-to-win-button" target="_blank" href="{{route('template')}}"><img class="button-img" src="{{asset('assets/images/Download-button.png')}}"></a> the postcard template.</o>
                            </div>   
                        </div>
                        <!-- end download postcard -->
                        <!-- colour postcard -->
                        <div class="col-12 col-md mb-5">
                            <div class="row col-12 col-md justify-content-center align-items-center mx-auto px-0">
                                    <h1 class="col-12 col-md-5 subtitle d-none d-md-block text-right pr-0">Step 2:</h1>
                                    <div class="col-12 col-md-7 text-center text-md-left px-0"><img class="how-to-win-image" src="{{asset('assets/images/Step-2-icon.png')}}"></div>
                            </div>
                            <div class="d-flex col-12 col-md justify-content-center align-items-start text-center mt-2 mx-auto step-description px-0">
                                <h1 class="col-2 subtitle d-block d-md-none">Step 2:</h1>
                                <p class="col-10 how-to-win-instruction">Colour and design the postcard based on your own creativity. Submissions can be done via hand-colouring or digital colouring.</p>
                            </div>   
                        </div>
                        <!-- end colour postcard -->

                        <!-- colour postcard -->
                        <div class="col-12 col-md mb-5">
                            <div class="row col-12 col-md justify-content-center align-items-center mx-auto px-0">
                                    <h1 class="col-12 col-md-5 subtitle d-none d-md-block text-right pr-0">Step 3:</h1>
                                    <div class="col-12 col-md-7 text-center text-md-left px-0"><img class="how-to-win-image" src="{{asset('assets/images/Step-3-icon.png')}}"></div>
                            </div>
                            <div class="d-flex col-12 col-md justify-content-center align-items-start text-center mt-2 mx-auto step-description px-0">
                                <h1 class="col-2 subtitle d-block d-md-none">Step 3:</h1>
                                <p class="col-10 how-to-win-instruction">Register and upload your submission <a href="#join-now"><button class="how-to-win-button text-center"><img class="button-img" src="{{asset('assets/images/Here-button.png')}}"></button></a></p>
                            </div>   
                        </div>
                        <!-- end colour postcard -->
                    </div>
                </div>
        </div>

        <!-- Desktop Only Building element  -->
        <div class="desktop-display text-left px-5"><img id="mobile-stone-grass-1" class="pr-2" src="{{asset('assets/images/Building-element.png')}}" /></div>
        <!-- End Desktop Only Building element  --> 


        <div id="postcard-template" class="text-center position-relative section mt-5 my-md-1 mb-0 mb-md-2">
            <img id="postcard-watercolor" class="position-absolute" src="{{asset('assets/images/mobile/Postcard-watercolour.png')}}"/>
            <div class="content-box">
            <h1 class="blue-title my-3 mb-5 mb-md-3">Postcard Template</h1>
            <div id="postcard-template-background">
                <img id="postcard-template-img" src="{{asset('assets/images/Postcard-template.png')}}"/>
                <div class="my-5 my-md-3">
                    <a id="download-postcard-button" href="{{route('template')}}">
                     <img class="desktop-display button-img cta-button" src="{{asset('assets/images/Download-button.png')}}">
                     <img class="mobile-display button-img cta-button" src="{{asset('assets/images/Download-button.png')}}">
                    </a>
                </div>
            </div>
            </div>
        </div>

        <!-- Mobile Only Grass & Stone  -->
        <div class="text-right mobile-stone-grass pr-5 my-n5 mb-5"><img id="mobile-stone-grass-1" src="{{asset('assets/images/mobile/Building-element.png')}}" /></div>
        <!-- End Mobile Only Grass & Stone  --> 

        <!-- Join Now -->
        <div id="join-now" class="mt-4 ">
            <div class="row justify-content-between align-items-end mb-3 no-gutters" id="title-row">
                <div class="col-3 col-md-2"><img src="{{asset('assets/images/Tree-element.png')}}"/></div>
                <div class="col-6 text-center"><h1 id="join-now-title" class="blue-title">Join Now</h1></div>
                <div class="col-3 col-md-2 text-right"><img src="{{asset('assets/images/Tree-element.png')}}"/></div>
            </div>

            <form class="content-box dropzone" method="POST" id="submitForm" action="{{route('store')}}/#join-now" enctype="multipart/form-data">
                @csrf
                <div class="form-group row mx-auto no-gutters mb-3">
                    <div class="col-12 col-md-8 mx-auto no-gutters justify-content-center">
                        <div class="col row d-flex no-gutters">
                                <div class="col-sm-12 col-md-6 my-4 my-md-2 pr-md-1">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp">
                                    <small id="nameHelp" class="form-text text-danger form-error">@error('name'){{$message}}@enderror</small>
                                </div>
                                <div class="col-sm-12 col-md-6 my-4 my-md-2 px-md-1">
                                <label for="contact">Mobile Number</label>
                                <input type="text" class="form-control" name="contact" id="contact" aria-describedby="contactHelp">
                                <small id="contactHelp" class="form-text text-danger form-error">@error('contact'){{$message}}@enderror</small>
                                </div>
                                <div class="col-sm-12 col-md-12 my-4 my-md-2">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                                <small id="emailHelp" class="form-text text-danger form-error">@error('email'){{$message}}@enderror</small>
                            </div>
                            <div class="col-sm-12 col-md-12 mt-4 mt-md-2">
                                <label for="mailing_address_line_1">Mailing Address</label>
                                <input type="text" class="form-control mb-md-2" id="mailing_address_line_1" name="mailing_address_line_1" aria-describedby="mailing_address_line_1Help" placeholder="Address Line 1">
                                <small id="mailing_address_line_1Help" class="form-text text-danger form-error">@error('mailing_address_line_1'){{$message}}@enderror</small>
                                <input type="text" class="form-control my-5 my-md-3" id="mailing_address_line_2" name="mailing_address_line_2" aria-describedby="mailing_address_line_2Help" placeholder="Address Line 2">
                                <small id="city_stateHelp" class="form-text text-danger form-error form-error">@error('mailing_address_line_2'){{$message}}@enderror</small>
                                <div class="row no-gutters my-md-2">
                                    <div class="col mr-5 mr-md-1">
                                        <input type="text" class="form-control" id="city_state" name="city_state" aria-describedby="city_stateHelp" placeholder="City State">
                                        <small id="city_stateHelp" class="form-text text-danger form-error">@error('city_state'){{$message}}@enderror</small>
                                    </div>   
                                    <div class="col ml-5 ml-md-1">
                                        <input type="text" class="form-control" id="zip_code" name="zip_code" aria-describedby="zip_codeHelp" placeholder="Zip Code">
                                        <small id="zip_codeHelp" class="form-text text-danger form-error">@error('zip_code'){{$message}}@enderror</small>
                                    </div>
                                </div>
                            </div>

                            <div id="pdpa-checkbox-desktop" class="w-100">
                                <label class="col-sm-12 col-md-12 d-flex form-check custom-checkbox">
                                    <input type="checkbox" value="1" name="checkbox">
                                    <span>I agree to the <u data-toggle="modal" data-target="#terms-and-condition">Terms and Conditions</u> and <u data-toggle="modal" data-target="#privacy-policy">Privacy Policy</u></span>
                                </label>
                                <div><small class="form-text text-danger form-error">@error('checkbox'){{$message}}@enderror</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mx-auto my-5 my-md-2 pl-md-2">
                        <div><label>Upload Submission</label></div>    
                        <div class="text-center my-0 row no-gutters align-items-center dz-default dz-message dropzoneDragArea" id="file-upload-box" >
                            <div class="col-12">
                               <div class="my-4">
                                   <img id="upload-icon" src="{{asset('assets/images/Upload-icon.png')}}" alt="">
                               </div> 
                               <div class="my-4">
                                <button class="my2 btn" id="file-upload-btn" type="button">
                                    <p class="subtitle">Upload File</p>
                                </button>
                                <input class="d-none" type="file" name="file" id="file">
                                <small class="form-text text-danger form-error">@error('file'){{$message}}@enderror</small>
                               </div>
                            </div>
                        </div>
                        <div class="dropzone-previews"></div>
                        
                    </div>

                <div id="pdpa-checkbox-mobile" class="w-100">
                <label class="d-md-none my-2 d-flex custom-checkbox form-check">
                    <input type="checkbox" value="1" name="checkbox">
                    <span>I agree to the <u data-toggle="modal" data-target="#terms-and-condition">Terms and Conditions</u> and <u data-toggle="modal" data-target="#privacy-policy">Privacy Policy</u></span>
                </label>
                </div>
                <div><small class="d-block form-text text-danger form-error">@error('checkbox'){{$message}}@enderror</small></div>

                <div class="text-center mx-auto">
                    <button type="submit" class="btn cta-button" id="submit-btn">
                        <img class="button-img" src="{{asset('assets/images/mobile/Submit-button.png')}}"/>
                    </button>
                </div>
            </form>
        </div>

        <!-- Mobile Only Grass & Stone  -->
        <div class="mobile-stone-grass">
            <br>
            <br>
            <br>
            <div class="mt-4 mobile-stone-grass d-flex justify-content-between">
                <div>
                    <img id="mobile-stone-grass-3" src="{{asset('assets/images/mobile/Stone-element-1.png')}}"/>
                </div>
                <div class="text-right align-self-end">
                    <img id="mobile-brush" src="{{asset('assets/images/mobile/Brush.png')}}"/>
                </div>
            </div>
        </div>
        <!-- End Mobile Only Grass & Stone  -->
        
        <div id="gallery" class="text-center position-relative section">
            <div class="content-box mt-0 mt-md-5">
                <h1 class="blue-title my-1 my-md-3 mb-2 mb-md-1">Gallery</h1>
                <p class="text-center mb-5 mb-md-4">Check out our gallery and sport your submission!</p>
                <div id="gallery-box" class="my-4 my-md-2">
                    <div id="gallery-box-inner" class="row no-gutters">
                        @foreach($images as $image)
                        <div class="col-4 col-md-3 d-flex align-items-center">
                            <img src="/storage/participants/<?php echo($image->file_name)?>" alt="">
                        </div>
                        @endforeach
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                        <div class="col-4 col-md-3">
                            <img src="{{asset('assets/images/image-outline.png')}}" alt="">
                        </div>
                
                    </div>
                </div>
            </div>
        </div>
        
        <!-- End Join Now -->

        <!-- Footer  -->
        <div id="footer" class="mx-auto pb-5 pb-md-3 position-relative">
            <div class="content-box mx-auto text-md-left d-flex flex-column flex-md-row align-items-md-end justify-content-between">
                <div class="order-0 order-md-2 my-4 my-md-2">
                    <p class="text-bold footer-title text-center text-md-left">Follow Us</p>
                    <div class="d-flex d-column justify-content-center justify-content-md-start">
                        <a href="https://www.facebook.com/KTOMalaysia/" target="_blank"><img class="px-5 px-md-0 footer-social-icon" src="{{asset('assets/images/FB-icon.png')}}" alt=""></a>
                        <a href="https://www.instagram.com/kto_malaysia/?hl=en" target="_blank"><img class="px-5 px-md-0 footer-social-icon" src="{{asset('assets/images/IG-icon.png')}}" alt=""></a>
                        <a href="https://www.youtube.com/user/KoreaTourismKL" target="_blank"><img class="px-5 px-md-0 footer-social-icon" src="{{asset('assets/images/Youtube-icon.png')}}" alt=""></a>
                    </div>
                </div>
                <!-- <div class="d-block my-3 d-md-none"></div> -->
                <div class="order-1 my-4 my-md-1">
                    <p class="text-bold text-center text-md-left">Korea Plaza</p>
                    <div class="d-flex flex-row flex-md-column align-items-start justify-content-between">
                        <p class="text-bold px-2 px-md-0 pl-md-0">Phone: 03-2072 2515</p>
                        <p class="text-bold px-2 px-md-0 pl-md-0 mb-0">Email: klkoreaplaza@gmail.com</p>
                    </div>
                    
                </div>
                <!-- <div class="d-block my-3 d-md-none"></div> -->
                <div class="order-2 order-md-0 my-4 my-md-2">
                    <p class="text-bold text-center text-md-left">Terms and Conditions</p>
                    <p class="text-bold text-center text-md-left">Sponsored by</p>
                    <div class="desktop-display">
                        <div class="footer-logos">
                            <div class="d-inline">                       
                                <img src="{{asset('assets/images/KTO-logo.png')}}" alt="">
                            </div>
                            <div class="d-inline">                       
                                <img src="{{asset('assets/images/IYK-logo.png')}}" alt="">
                            </div>
                            <div class="d-inline">                       
                                <img src="{{asset('assets/images/Jeju-logo.png')}}" alt="">
                            </div>
                            <div class="d-inline">                       
                                <img src="{{asset('assets/images/Gangwon-logo.png')}}" alt="">
                            </div>
                            <div class="d-inline">                       
                                <img src="{{asset('assets/images/BTO-logo.png')}}" alt="">
                            </div>
                        </div>
                    </div>

                    <div class="mobile-display mobile-logos mx-auto">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-4 text-center">                       
                                <img src="{{asset('assets/images/mobile/KTO-logo.png')}}" alt="">
                            </div>
                            <div class="col-3 text-center">                       
                                <img src="{{asset('assets/images/mobile/IYK-logo.png')}}" alt="">
                            </div>
                        </div>
                        <div class="d-flex justify-content-center align-items-end mt-5">
                            <div class="text-center px-4">                       
                                <img src="{{asset('assets/images/mobile/Jeju-logo.png')}}" alt="">
                            </div>
                            <div class="text-center px-4">                       
                                <img src="{{asset('assets/images/mobile/Gangwon-logo.png')}}" alt="">
                            </div>
                            <div class="text-center flex-shrink-3 px-5">                       
                                <img src="{{asset('assets/images/mobile/BTO-logo.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                    
               
            </div>
                <br>
                <p class="content-box text-bold text-center text-md-left pb-5">&copy; 2020 All Rights Reserved</p>  

            <!-- Mobile Only Grass & Stone  -->
           <img class="position-absolute mobile-display" id="mobile-footer-img" src="{{asset('assets/images/mobile/footer.png')}}"/>
            <!-- End Mobile Only Grass & Stone  --> 

        </div>

        @include('termsandcondition')
        @include('privacypolicy')
        
        <!-- End Footer  -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
        <!-- <script src="{{asset('js/dropzone.min.js')}}"></script> -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>


            $(document).ready(function() {
                // Optimalisation: Store the references outside the event handler:
                var $window = $(window);

                function checkWidth() {
                    var windowsize = $window.width();
                    if (windowsize > 780) {
                        $("#pdpa-checkbox-desktop").remove();
                    }
                    else{
                        $("#pdpa-checkbox-mobile").remove();
                    }
                }

                // file upload button event
                $('#file-upload-btn').click(function(){
                    $('#file').click();
                });

                // Execute on load
                // Bind event listener
                // $(window).resize(checkWidth);
                // Dropzone.autoDiscover = false;
                // let token = $('meta[name="csrf-token"]').attr('content');
                // var fileDropzone = new Dropzone('div#file-upload-box',{
                //     paramName : "file",
                //     url: "{{route('store')}}",
                //     previewsContainer: 'div.dropzone-previews',
                //     addRemoveLinks: true,
                //     parallelUploads: 1,
                //     maxFiles: 1,
                //     maxFilesize: 5,
                //     autoProcessQueue: false,
                //     params:{
                //         _token: token
                //     },
                //     init: function(){
                //         var myDropzone = this;

                //         $("#submit-btn").click(function(e) {
                //             // Make sure that the form isn't actually being sent.
                //             e.preventDefault();
                //             e.stopPropagation();
                //             myDropzone.processQueue();
                //         });

                //         this.on("sendingmultiple", function(data, xhr, formData) {
                //             formData.append("name", $("#name").val());
                //             formData.append("contact", $("#contact").val());
                //             formData.append("email", $("#email").val());
                //             formData.append("mailing_address_line_1", $("#mailing_address_line_1").val());
                //             formData.append("mailing_address_line_2", $("#mailing_address_line_2").val());
                //             formData.append("city_state", $("#city_state").val());
                //             formData.append("zip_code", $("#zip_code").val());
                //             formData.append("checkbox", $("input[type='checkbox']").val());
                //         });

                //         this.on("success", function(data){
                //             alert("Sent!");
                //             console.log(data);
                //         });
                //   }
                // })
            });
        </script>
    </body>
</html>
