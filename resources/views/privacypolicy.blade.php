<div class="modal" tabindex="-1" role="dialog" id="privacy-policy">
  <div class="modal-dialog modal-dialog-centered pdpa-modal" role="document">
    <div class="modal-content">
      <div class="modal-header align-items-center">
      <h5 class="text-bold pdpa-title">Privacy Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <p class="close-icon"><span aria-hidden="true">&times;</span></p>
        </button>
        
      </div>
      <div class="modal-body">
            <p class="text-bold pdpa-text">Contest Eligibility</p>
            <p class="pdpa-text">1. To participate in the #colouryourkorea Colouring Contest (the “Contest”), participants (the “Participants”) must be a Malaysian residing in Malaysia.</p>
            <p class="pdpa-text">2. Korea Tourism Organization Malaysia (the “Organiser”) reserves the right to disqualify any Participants without notice at any point of the contest or after should he/she be found to have not fulfilled the pre-requisites of the contest prior to participation.</p>
            <p class="text-bold pdpa-text">Joining the Contest</p>
            <p class="pdpa-text">3. The Contest will run from 2 Nov 2020 to 30 Nov 2020.</p>
            <p class="pdpa-text">4. The Organiser reserves the right to amend the Contest period at any time without prior notice.</p>
            <p class="pdpa-text">5. To participate in this Contest, the Participants are required to colour and design the postcard based on their creativity. It is compulsory to complete all the designs in the postcard before being evaluated.</p>
            <p class="pdpa-text">6. Participants must register and upload artwork on www.coloryourkorea.my microsite.</p>
            <p class="pdpa-text">7. Participants can only submit one artwork per registration. Multiple submissions will be eliminated.</p>
            <p class="text-bold pdpa-text">Winner Selection</p>
            <p class="pdpa-text">8. 3 winners will be selected at the end of the Contest. The winner announcement will be made on 16 Dec 2020 on the Organiser’s Facebook page and the winners’ will be notified via emails.</p>
            <p class="pdpa-text">9. Winners will be selected by the Organiser and all decisions will be final.</p>
            <p class="pdpa-text">10. Submitted artworks will not be available for commercial purposes and the artwork copyright will be reserved solely for the Organiser’s marketing purposes only.</p>
            <p class="pdpa-text">11. In order to respond to the participant’s questions, fulfil the participant’s requests or manage interactive content in the Contest, it may be necessary to ask for personal information such the Participant’s name and email address. The Organiser may use said information to respond to the Participant’s requests, or to contact the participant via e-mail to inform the Participant of news and other promotions. However, unless compelled by applicable legislation, The Organiser will not provide said information to a third party without the Participant’s permission.</p>
            <p class="pdpa-text">11. In addition to the personal information as mentioned in the previous clause, certain technology may be used to collect certain technical information like the Participant’s Internet protocol address, the Participant’s computer's operating system, the Participant’s browser type, traffic patterns and the address of any referring web site.</p>
            <p class="pdpa-text">12. The Organiser will erase any incomplete, inaccurate or outdated personal data retained by the Organiser in connection with the operation of this Contest.</p>
            <p class="text-bold pdpa-text">Prizes</p>
            <p class="pdpa-text">13. Prizes for the winners are as the following:</p>
            <p class="pdpa-text">Grand Prize: Galaxy Tab S7 (worth RM3,299)</p>
            <p class="pdpa-text">10 X Consolation Prizes: LANEIGE Essential Care Trial Set (worth RM1,600)</p>
            <p class="pdpa-text">Participation Prizes: Care Package with Innisfree Hand Cream, Mask Case, and Covid Care Kit (worth RM60 each)</p>
            <p class="pdpa-text">14. Prizes are non-transferable, non-exchangeable for cash prizes, and non-refundable.</p>
            <p class="pdpa-text">15. In the event that the Organiser is unable to contact a winner, the Organiser reserves the right to select another winner based on the judging criteria.</p>
        <p class="text-bold pdpa-text">Other Terms</p>
        <p class="pdpa-text">16. By taking part in this contest, the Participants understand and accept without condition that the Organiser may elect to use the Participants’ personal information and artwork for marketing purposes in a manner that deems fit whilst according the Participants’ privacy or confidentiality with the highest priority.</p>
        <p class="pdpa-text">17. The Organiser shall not be liable for any loss of chance arising from any defect, malfunction or failure of any telephone network/lines, computer systems, servers, computer equipment, software used in connection with the Contest.</p>
        <p class="pdpa-text">18. The Organiser may, with or without prior notice, terminate any of the rights granted by these Terms and Conditions. Any changes are effective immediately upon posting to the Contest page and release of notice of such change.</p>
        <p class="pdpa-text">19. The Organiser reserves the right to cancel, suspend or amend the rules and prizes of the contest without prior notice. Any changes will be posted either on the Organiser’s Facebook Page or in the Terms and Conditions.</p>
        <p class="pdpa-text">20. The Organiser retains all proprietary rights to the intellectual property contained within the Contest, and owns the copyright to all contents within.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close-modal-button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>