<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ParticipantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ParticipantController::class, 'index']);

Route::post('store', [ParticipantController::class, 'upload'])->name('store');
Route::post('upload', [ParticipantController::class, 'fileUpload'])->name('upload');

Route::get('thankyou',function () {
    return view('thankyou'); 
})->name('thankyou');


Route::get('checkbox', function () {
    return view('checkbox');
});

// Download Template file
Route::get('template', function () {
    return response()->download(storage_path("app/public/Postcard-template.png"));
})->name('template');