<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Participant;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ParticipantController extends Controller
{

    public function index(){

        $images = Participant::select("file_name")->get();
        
        return view('home', ['images'=>$images]);
    }

    public function create()
    {
        return view('thankyou');
    }

    public function upload(Request $request){
        $request->validate([
                'name' => 'required|max:255',
                'contact' => ['required','unique:participants','regex:/^6?01\d{8,9}$/','max:255'],
                'email' => ['required','unique:participants','regex:/^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$/','max:255'],
                'mailing_address_line_1' => 'required|max:255',
                'mailing_address_line_2' => 'max:255',
                'city_state' => 'required',
                'zip_code' => 'required',
                'file' => 'required|mimes:png,jpg,pdf|max:5000',
                'checkbox'=>'accepted'
            ]);

            $extension = $request->file->extension();
            

            // Validate the value...
            $participant = new Participant;
            $participant->name = $request->name;
            $participant->contact = $request->contact;
            $participant->email = $request->email;
            $participant->mailing_address_line_1 = $request->mailing_address_line_1;
            $participant->mailing_address_line_2 = $request->mailing_address_line_2;
            $participant->city_state = $request->city_state;
            $participant->zip_code = $request->zip_code;
            
            $participant->save();

            $newFileName = $participant->id.".".$extension;
            $participant->file_name = $newFileName;
            $participant->save();

            $request->file->move(storage_path('app/public/participants'), $newFileName);
           
            // return redirect('thankyou');   
            return response('200');   
    }

    public function fileUpload(Request $request){
        $request->validate([

            'file' => 'required|max:5048',

        ]);

        try{

            $fileName = time().'.'.$request->file->extension();  

            $extension = $request->file->extension();
    
            dd($extension);
            $request->file->move(storage_path('participants'), $fileName);

    

            return back()

                ->with('success','You have successfully upload file.')

                ->with('file',$fileName)
                ->with('extension',$extension);
        }catch(Throwable $err){
            report($err);
        }
    }
}
